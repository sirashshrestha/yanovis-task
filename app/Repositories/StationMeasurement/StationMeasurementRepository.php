<?php

namespace App\Repositories\StationMeasurement;

use App\Models\StationMeasurement;

/**
 * Class StationMeasurementRepository
 * @package App\Repositories\StationMeasurement
 */
class StationMeasurementRepository implements StationMeasurementInterface
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $conditions
     * @return mixed
     * returns a single row for the matching conditions
     */
    public function findRow($conditions)
    {
        $query = StationMeasurement::where('id', '!=', 0);
        if (count($conditions) > 0) {
            foreach ($conditions as $key => $condition) {
                $query = $query->where($key, '=', $condition);
            }
        }
        return $query->first();
    }

    /**
     * @param StationMeasurement $stationMeasurement
     * @param                    $updateArray
     * @return bool|mixed
     * updates a single row
     */
    public function updateRow(StationMeasurement $stationMeasurement, $updateArray)
    {
        return $stationMeasurement->update($updateArray);
    }

    /**
     * @param $insertArray
     * @return mixed
     * insert a single row
     */
    public function insertRow($insertArray)
    {
        return StationMeasurement::create($insertArray);
    }
}