<?php

namespace App\Repositories\StationMeasurement;

use App\Models\StationMeasurement;

/**
 * Interface StationMeasurementInterface
 * @package App\Repositories\StationMeasurement
 */
interface StationMeasurementInterface
{
    /**
     * @param $conditions
     * @return mixed
     */
    public function findRow($conditions);

    /**
     * @param StationMeasurement $stationMeasurement
     * @param                    $updateArray
     * @return mixed
     */
    public function updateRow(StationMeasurement $stationMeasurement, $updateArray);

    /**
     * @param $insertArray
     * @return mixed
     */
    public function insertRow($insertArray);
}
