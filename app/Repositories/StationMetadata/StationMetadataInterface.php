<?php

namespace App\Repositories\StationMetadata;

use App\Models\StationMetadata;

/**
 * Interface StationMetadataInterface
 * @package App\Repositories\StationMetadata
 */
interface StationMetadataInterface
{
    /**
     * @param $conditions
     * @return mixed
     */
    public function findRow($conditions);

    /**
     * @param StationMetadata $stationMetadata
     * @param                 $updateArray
     * @return mixed
     */
    public function updateRow(StationMetadata $stationMetadata, $updateArray);

    /**
     * @param $insertArray
     * @return mixed
     */
    public function insertRow($insertArray);

    /**
     * @param $params
     * @return mixed
     */
    public function paginatedRows($params);
}
