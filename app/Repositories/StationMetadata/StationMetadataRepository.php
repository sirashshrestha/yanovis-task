<?php

namespace App\Repositories\StationMetadata;

use App\Models\StationMetadata;

/**
 * Class StationMetadataRepository
 * @package App\Repositories\StationMetadata
 */
class StationMetadataRepository implements StationMetadataInterface
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $conditions
     * @return mixed
     * returns a single row for the matching conditions
     */
    public function findRow($conditions)
    {
        $query = StationMetadata::where('id', '!=', 0);
        if (count($conditions) > 0) {
            foreach ($conditions as $key => $condition) {
                $query = $query->where($key, '=', $condition);
            }
        }
        return $query->first();
    }

    /**
     * @param StationMetadata $stationMetadata
     * @param                 $updateArray
     * @return bool|mixed
     * update a single row
     */
    public function updateRow(StationMetadata $stationMetadata, $updateArray)
    {
        return $stationMetadata->update($updateArray);
    }

    /**
     * @param $insertArray
     * @return mixed
     * insert a single row
     */
    public function insertRow($insertArray)
    {
        return StationMetadata::create($insertArray);
    }

    /**
     * @param $params
     * @return array|mixed
     * returns the paginated list with the matching conditions
     */
    public function paginatedRows($params)
    {
        $page         = !empty($params["page"]) ? $params["page"] : 1;
        $take         = !empty($params["rows_per_page"]) ? $params["rows_per_page"] : 1;
        $skip         = ($page - 1) * $take;
        $sortByColumn = !empty($params["sort_by_column"]) ? $params["sort_by_column"] : 'id';
        $sortByType   = !empty($params["sort_by_type"]) ? $params["sort_by_type"] : 'asc';
        $query        = StationMetadata::where('id', '!=', 0);
        if (!empty($params["name"])) {
            $query = $query->where('name_d', 'like', '%' . $params["name"] . '%');
        }
        return [
            'page'             => $page,
            'rows_per_page'    => $take,
            'sorted_by_column' => $sortByColumn,
            'sorted_by_type'   => $sortByType,
            'rows'             => $query->count(),
            'data'             => $query->skip($skip)->take($take)->orderBy($sortByColumn, $sortByType)->get()
        ];
    }
}