<?php

namespace App\Console\Commands;

use App\Services\StationMeasurementService;
use Illuminate\Console\Command;

/**
 * @property StationMeasurementService stationMeasurementService
 */
class SyncStationMeasurements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "sync:station_measurements {SCODE=no}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Stations Measurements from Meteo Webservice to the DB.';

    /**
     * Create a new command instance.
     *
     * @param StationMeasurementService $stationMeasurementService
     */
    public function __construct(StationMeasurementService $stationMeasurementService)
    {
        parent::__construct();
        $this->stationMeasurementService = $stationMeasurementService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* Read the Station Measurements from the API */
        $contents = $this->stationMeasurementService->readStationMeasurements($this->argument('SCODE'));
        if (!$contents) {
            print_r("No Data in the API");
        }
        /* Store and Update the Station Measurements Data received from the API */
        $this->stationMeasurementService->storeAndUpdateStationMeasurementService($contents);
        return;
    }
}
