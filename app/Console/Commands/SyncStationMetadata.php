<?php

namespace App\Console\Commands;

use App\Services\StationMetadataService;
use Illuminate\Console\Command;

/**
 * @property StationMetadataService stationMetadataService
 */
class SyncStationMetadata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "sync:station_metadata {SCODE=no}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Stations Metadata from Meteo Webservice to the DB.';

    /**
     * Create a new command instance.
     *
     * @param StationMetadataService $stationMetadataService
     */
    public function __construct(StationMetadataService $stationMetadataService)
    {
        parent::__construct();
        $this->stationMetadataService = $stationMetadataService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contents = $this->stationMetadataService->readStationMetadata();
        if (!$contents) {
            print_r("No Data in the API");
        }
        $this->stationMetadataService->storeAndUpdateStationMetadataService($contents, $this->argument('SCODE'));
        return;
    }
}
