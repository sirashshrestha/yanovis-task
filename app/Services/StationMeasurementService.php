<?php

namespace App\Services;

use App\Repositories\StationMeasurement\StationMeasurementInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class StationMeasurementService
 * @property StationMeasurementInterface stationMeasurementInterface
 * @package App\Services
 */
class StationMeasurementService
{
    /**
     * StationMeasurementService constructor.
     * @param StationMeasurementInterface $stationMeasurementInterface
     */
    public function __construct(StationMeasurementInterface $stationMeasurementInterface)
    {
        $this->stationMeasurementInterface = $stationMeasurementInterface;
    }

    /**
     * @param $scode
     * @return bool|mixed
     * read Station Metadata from the API
     */
    public function readStationMeasurements($scode)
    {
        $endpoint = env('API_URL', "http://dati.retecivica.bz.it/services/meteo/v1/") . 'sensors';
        $client   = new \GuzzleHttp\Client();
        $queries  = $scode != 'no' ? ['station_code' => $scode] : [];
        $response = $client->request('GET', $endpoint, ['query' => $queries]);
        if ($response->getStatusCode() != 200) {
            return false;
        }
        $contents = $response->getBody()->getContents();
        return json_decode($contents);
    }

    /**
     * @param $inputData
     * store and Update the data received from the api after formatting
     */
    public function storeAndUpdateStationMeasurementService($inputData)
    {
        $inputData = json_decode(json_encode($inputData), true);
        if (count($inputData) > 0) {
            print_r('Total Rows from API: ' . count($inputData));
            $addedRows   = 0;
            $deletedRows = 0;
            foreach ($inputData as $key => $inputDatum) {
                $row                = [];
                $row['value']       = !empty($inputDatum["VALUE"]) ? trim($inputDatum["VALUE"]) : null;
                $row['desc_d']      = !empty($inputDatum["DESC_D"]) ? trim($inputDatum["DESC_D"]) : null;
                $row['desc_i']      = !empty($inputDatum["DESC_I"]) ? trim($inputDatum["DESC_I"]) : null;
                $row['desc_l']      = !empty($inputDatum["DESC_L"]) ? trim($inputDatum["DESC_L"]) : null;
                $row['unit']        = !empty($inputDatum["UNIT"]) ? trim($inputDatum["UNIT"]) : null;
                $inputDatumSCODE    = !empty($inputDatum["SCODE"]) ? trim($inputDatum["SCODE"]) : null;
                $inputDatumType     = !empty($inputDatum["TYPE"]) ? trim($inputDatum["TYPE"]) : null;
                $carbonDate         = !empty($inputDatum["DATE"]) ? Carbon::parse(trim($inputDatum["DATE"])) : null;
                $stationMeasurement = null;
                $row['date']        = $carbonDate;
                if (!empty($inputDatumSCODE) && !empty($inputDatumType)) {
                    $conditions         = [
                        'scode' => $inputDatumSCODE,
                        'type'  => $inputDatumType
                    ];
                    $stationMeasurement = $this->stationMeasurementInterface->findRow($conditions);
                }
                DB::beginTransaction();
                if (!empty($stationMeasurement)) {
                    $this->stationMeasurementInterface->updateRow($stationMeasurement, $row);
                    $deletedRows++;
                } else {
                    $row['scode'] = $inputDatumSCODE;
                    $row['type']  = $inputDatumType;
                    $this->stationMeasurementInterface->insertRow($row);
                    $addedRows++;
                }
                DB::commit();
            }
            print_r('   Rows Updated: ' . $deletedRows);
            print_r('   Rows Inserted: ' . $addedRows);
        }
    }
}
