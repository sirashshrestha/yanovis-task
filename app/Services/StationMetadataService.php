<?php

namespace App\Services;

use App\Repositories\StationMetadata\StationMetadataInterface;
use App\Models\StationMetadata;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property StationMetadata          stationMetadata
 * @property StationMetadataInterface stationMetadataInterface
 */
class StationMetadataService
{
    public function __construct(StationMetadataInterface $stationMetadataInterface, StationMetadata $stationMetadata)
    {
        $this->stationMetadata          = $stationMetadata;
        $this->stationMetadataInterface = $stationMetadataInterface;
    }

    /* Read Station Measurements from the API */
    public function readStationMetadata()
    {
        $endpoint = env('API_URL', "http://dati.retecivica.bz.it/services/meteo/v1/") . 'stations';
        $client   = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['query' => []]);
        if ($response->getStatusCode() != 200) {
            return false;
        }
        $contents = $response->getBody()->getContents();
        return json_decode($contents);
    }

    /* Store and Update the data from the api after formatting it */
    public function storeAndUpdateStationMetadataService($inputData, $scode)
    {
        $inputData   = json_decode(json_encode($inputData), true);
        $inputData   = $inputData['features'];
        $addedRows   = 0;
        $updatedRows = 0;
        if (count($inputData) > 0) {
            print_r('Total Rows from API: ' . count($inputData));
            foreach ($inputData as $key => $inputDatum) {
                $row = [];
                if (!empty($inputDatum["properties"])) {
                    $properties = $inputDatum["properties"];
                    if ($scode != 'no' && $scode != $properties['SCODE']) {
                        continue;
                    }
                    $scode1          = !empty($properties["SCODE"]) ? trim($properties["SCODE"]) : null;
                    $conditions      = [
                        'scode' => $scode1
                    ];
                    $stationMetadata = $this->stationMetadataInterface->findRow($conditions);
                    $row['name_d']   = !empty($properties["NAME_D"]) ? trim($properties["NAME_D"]) : null;
                    $row['name_i']   = !empty($properties["NAME_I"]) ? trim($properties["NAME_I"]) : null;
                    $row['name_l']   = !empty($properties["NAME_L"]) ? trim($properties["NAME_L"]) : null;
                    $row['name_e']   = !empty($properties["NAME_E"]) ? trim($properties["NAME_E"]) : null;
                    $row['alt']      = !empty($properties["ALT"]) ? trim($properties["ALT"]) : null;
                    $row['lat']      = !empty($properties["LAT"]) ? trim($properties["LAT"]) : null;
                    $row['lon']      = !empty($properties["LONG"]) ? trim($properties["LONG"]) : null;
                }
                $row['type'] = !empty($inputDatum["type"]) ? trim($inputDatum["type"]) : null;
                if (!empty($inputDatum['geometry'])) {
                    $geometry         = $inputDatum['geometry'];
                    $row['geom_type'] = !empty($geometry["type"]) ? trim($geometry["type"]) : null;
                    $row['geom_coordinates'] = null;
                    if (strtolower($row['geom_type']) == "point") {
                        $geomCoordinates         = !empty($geometry["coordinates"]) ? $row['geom_type'] . '(' . implode(", ", $geometry["coordinates"]) . ')' : null;
                        $point                   = Point::fromWKT($geomCoordinates);
                        $row['geom_coordinates'] = DB::raw("ST_GeomFromText('{$point->toWkt()}')");
                    }
                }
                DB::beginTransaction();
                if (!empty($stationMetadata)) {
                    $this->stationMetadataInterface->updateRow($stationMetadata, $row);
                    $updatedRows++;
                } else {
                    $row['scode'] = $scode1;
                    $this->stationMetadataInterface->insertRow($row);
                    $addedRows++;
                }
                DB::commit();
            }
            print_r('   Rows Updated: ' . $updatedRows);
            print_r('   Rows Inserted: ' . $addedRows);
        }
    }

    public function list(Request $request)
    {
        $params = $request->all();
        return $this->stationMetadataInterface->paginatedRows($params);
    }

    public function detail($id, Request $request)
    {
        $conditions      = [
            'id' => $id
        ];
        $stationMetadata = $this->stationMetadataInterface->findRow($conditions);
        if (empty($stationMetadata)) {
            abort(404);
        }
        return [
            'data' => $stationMetadata
        ];
    }
}

