<?php

namespace App\Traits;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

trait RestExceptionHandlerTrait
{
    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request    $request
     * @param \Exception $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, \Exception $exception)
    {
        // dd($exception);
        DB::rollback();
        if ($exception instanceof ModelNotFoundException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Model not found', 'errors' => []],
                $exception->getStatusCode()
            );
        } elseif ($exception instanceof UnauthorizedHttpException) {
            if ($exception->getMessage() == 'Token has expired') {
                $responseData = response()->json(
                    ['message' => 'Failure', 'title' => 'Unauthorized request', 'errors' => []],
                    403
                );
                return $responseData;
            } else {
                $responseData = response()->json(
                    ['message' => 'Failure', 'title' => 'Unauthorized request', 'errors' => []],
                    403
                );
            }
        } elseif ($exception instanceof AuthenticationException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Unauthorized request', 'errors' => []],
                403
            );
        } elseif ($exception instanceof MethodNotAllowedHttpException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Method not allowed', 'errors' => []],
                $exception->getStatusCode()
            );
        } elseif ($exception instanceof NotFoundHttpException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Page not found', 'errors' => []],
                $exception->getStatusCode()
            );
        } elseif ($exception instanceof QueryException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Server Error', 'errors' => []],
                400
            );
        } elseif ($exception instanceof ValidationException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Validation error', 'errors' => $exception->errors()],
                400
            );
        } elseif ($exception instanceof PostTooLargeException) {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'PostTooLargeException', 'errors' => []],
                400
            );
        } else {
            $responseData = response()->json(
                ['message' => 'Failure', 'title' => 'Generic Error', 'errors' => []],
                400
            );
        }
        return $responseData;
    }
}