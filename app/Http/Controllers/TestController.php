<?php

namespace App\Http\Controllers;

use App\Jobs\SyncJob;
use App\Services\StationMetadataService;
use App\Services\StationMeasurementService;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function __construct(StationMetadataService $stationMetadataService, StationMeasurementService $stationMeasurementService)
    {
        $this->stationMeasurementService = $stationMeasurementService;
        $this->stationMetadataService = $stationMetadataService;
    }

    public function test(){
        dispatch(new SyncJob());
        dd('here');
    }
}
