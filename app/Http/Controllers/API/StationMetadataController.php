<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Services\StationMetadataService;
use Illuminate\Http\Request;

/**
 * @property StationMetadataService stationMetadataService
 */
class StationMetadataController extends BaseController
{
    /**
     * StationMetadataController constructor.
     * @param StationMetadataService $stationMetadataService
     */
    public function __construct(StationMetadataService $stationMetadataService)
    {
        $this->stationMetadataService = $stationMetadataService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * returns the list of Metadata API with filter and pagination
     */
    public function list(Request $request)
    {
        $request->validate([
            'page'           => 'required|integer|not_in:0',
            'rows_per_page'  => 'required|integer|not_in:0',
            'sort_by_column' => 'required|in:name_d,name_i,name_l,name_e,scode,alt',
            'sort_by_type'   => 'required|in:ASC,DESC,asc,desc'
        ]);
        return $this->response('Success.', 'Station Metadata', $this->stationMetadataService->list($request), 200);
    }

    /**
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * returns the single row the the corresponding if of the Station Metadata
     */
    public function detail($id, Request $request)
    {
        return $this->response('Success.', 'Station Metadata Detail', $this->stationMetadataService->detail($id, $request), 200);
    }
}
