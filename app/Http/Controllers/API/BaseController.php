<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function response($message = "OK", $title, $data = [], $statusCode = 200)
    {
        $returnData = array_merge([
            'message' => $message,
            'title'   => $title
        ], $data);
        return response()->json($returnData, $statusCode);
    }
}
