<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StationMeasurement extends Model
{
    protected $table = 'station_measurements';
    protected $guarded = ['id'];

    public function bulkInsert($rows)
    {
        return $this->insert($rows);
    }
}
