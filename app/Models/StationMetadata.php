<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StationMetadata extends Model
{
    protected $table = 'station_metadata';
    protected $guarded = ['id'];

    public function bulkInsert($rows)
    {
        return $this->insert($rows);
    }
}
