<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_metadata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('type', 20);
            $table->char('geom_type', 20);
            $table->point('geom_coordinates');
            $table->spatialIndex('geom_coordinates');
            $table->char('scode', 7);
            $table->string('name_d')->nullable();
            $table->string('name_i')->nullable();
            $table->string('name_l')->nullable();
            $table->string('name_e')->nullable();
            $table->decimal('alt')->nullable();
            $table->decimal('lon', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_metadata');
    }
}
