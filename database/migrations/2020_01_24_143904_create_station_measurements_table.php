<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_measurements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('scode', 10);
            $table->char('type', 10);
            $table->string('desc_d')->nullable();
            $table->string('desc_i')->nullable();
            $table->string('desc_l')->nullable();
            $table->char('unit', 10)->nullable();
            $table->dateTimeTz('date')->nullable();
            $table->float('value')->nullable();
            $table->unique(['scode', 'type']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_measurements');
    }
}
